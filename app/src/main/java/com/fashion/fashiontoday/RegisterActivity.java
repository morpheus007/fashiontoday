package com.fashion.fashiontoday;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//This activity is only called when the user uses the app for the first time
//After registering this activity is never called again when the app starts
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    EditText userName, enterPassword, confirmPassword;
    Button registerButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.register_screen);

        //Setting username and password for the first time use
        userName = (EditText) findViewById(R.id.userName);
        enterPassword = (EditText) findViewById(R.id.enterPassword);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);

        registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.registerButton) {

            //This checks if the user doesn't enter blank username or password
            //No need to check if  confirmPassword is blank as the next validation checks
            //for the enterPassword and confirmPassword to match
            if (enterPassword.getText().toString().trim().equals("") || userName.getText().toString().trim().equals("")) {
                Toast.makeText(this, R.string.blank_value_validate, Toast.LENGTH_LONG).show();

                //reset the text fields
                userName.setText("");
                enterPassword.setText("");
                confirmPassword.setText("");

            }
            //This statement checks if the user hasn't entered any white spaces in the username or password
            else if(enterPassword.getText().toString().contains(" ") ||
                    userName.getText().toString().contains(" ")){

                Toast.makeText(this, R.string.white_space_validate, Toast.LENGTH_LONG).show();

                //reset the text fields
                userName.setText("");
                enterPassword.setText("");
                confirmPassword.setText("");

            }
            //This statement checks if the confirmPassword and enterPassword match and if they don't
            //then the both password fields are set to null
            else if (!enterPassword.getText().toString().equals(confirmPassword.getText().toString())) {
                Toast.makeText(this, R.string.password_match_validate, Toast.LENGTH_LONG).show();
                enterPassword.setText("");
                confirmPassword.setText("");

            }
            //If the above validations are done then we can store the user information in the database
            else {

                //This creates a database using a database handler
                DBHandler dbHandler = new DBHandler(this, null);

                //This will check if there is any old user entry of user in database
                if (!dbHandler.databaseToString().equals("")) {

                    //This function deletes the old entry of user from the database
                    dbHandler.deleteUser();
                }

                //Add the values to the user model
                User user = new User();
                user.setUserName(userName.getText().toString());
                user.setPassword(enterPassword.getText().toString());

                //Add User object to the database
                dbHandler.addUser(user);

                //Check if the shirts or pants directory aren't empty
                boolean shirtEmpty, pantEmpty;

                //Call directory checker for checking the directory if they are empty
                shirtEmpty = UtilClass.isDirEmpty(UtilClass.getShirtsDirectory());
                pantEmpty = UtilClass.isDirEmpty(UtilClass.getPantsDirectory());

                //If any of the shirts or pants directories are empty then call AddClothes Activity
                //Else it will call the main activity i.e. the NavDrawer activity
                if (shirtEmpty || pantEmpty) {
                    Intent addClothes = new Intent(RegisterActivity.this, AddClothes.class);
                    finish();
                    startActivity(addClothes);

                } else {
                    Intent mainActivity = new Intent(RegisterActivity.this, NavDrawerActivity.class);
                    startActivity(mainActivity);
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }


}

