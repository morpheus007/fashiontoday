package com.fashion.fashiontoday;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SuggestionsFragment extends Fragment implements View.OnClickListener {
    Button nextSuggestion, saveSuggestion;

    List<String> shirts, pants;

    String shirtPath, pantPath;

    ImageView shirtSuggest, pantSuggest;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.suggestions, container, false);

        //ArrayList to store the paths of shirts and pants
        shirts = new ArrayList<>();
        pants = new ArrayList<>();

        shirtSuggest = (ImageView) view.findViewById(R.id.shirtSuggest);
        pantSuggest = (ImageView) view.findViewById(R.id.pantSuggest);

        nextSuggestion = (Button) view.findViewById(R.id.nextSuggestion);
        saveSuggestion = (Button) view.findViewById(R.id.saveSuggestion);

        nextSuggestion.setOnClickListener(this);
        saveSuggestion.setOnClickListener(this);


        for (File file : UtilClass.getShirtsDirectory().listFiles()) {
            shirts.add(file.getAbsolutePath());
        }
        for (File file : UtilClass.getPantsDirectory().listFiles()) {
            pants.add(file.getAbsolutePath());
        }
        //generate a new suggestion of shirts and pants
        newSuggestion();

        return view;
    }

    //This function will take a list of strings and generate a random position
    //It will then return the path corresponding to that random position
    private String getPath(List<String> list) {
        Random random = new Random();

        //New Random Position
        int randomPosition = random.nextInt() % list.size();

        //This will avoid any negative random number
        //If negative it will generate positive number
        if (randomPosition < 0) {
            randomPosition *= -1;
        }

        return list.get(randomPosition);
    }

    //This function will create a pair of random shirts and pants
    public void newSuggestion() {

       /* This function will check if all the combinations of shirts and pants have occurred
        if all the combinations have occurred then both shirt suggestions and pant suggestions
        will be null and it will return.UtilClass.getFileCombinations will get the size of
        the combinations array that have been occurred*/
        if (UtilClass.getFileCombinations().size() >= shirts.size() * pants.size()) {
            shirtSuggest.setImageBitmap(null);
            pantSuggest.setImageBitmap(null);
            return;
        }

        do {
            shirtPath = getPath(shirts);
            pantPath = getPath(pants);
        }

        //This will loop till we get a unique combination
        while (UtilClass.getFileCombinations().contains(shirtPath + " " + pantPath));

        //These will get shirt and pant image files from paths and decode it to bitmap
        shirtSuggest.setImageBitmap(BitmapFactory.decodeFile(shirtPath));
        pantSuggest.setImageBitmap(BitmapFactory.decodeFile(pantPath));

    }

    @Override
    public void onClick(View v) {
        //Again this if statement will ensure that all the combinations are not exhausted
        //If this condition is met then only the click events will work
        if (UtilClass.getFileCombinations().size() < shirts.size() * pants.size()) {

            //As any of the two buttons are clicked we will save this path to our file
            //And this combination will never be repeated
            UtilClass.writeFile(shirtPath + " " + pantPath);

            switch (v.getId()) {

                //This button means that the user disliked the combination
                //it will just break the switch case and create a new suggestion
                case R.id.nextSuggestion:
                    break;

                case R.id.saveSuggestion:
                    //This will combine the two bitmap images as a bookmark
                    Bitmap bookmark = UtilClass.combineBitmap(BitmapFactory.decodeFile(shirtPath), BitmapFactory.decodeFile(pantPath));

                    //This function will store the combined bitmap in bookmarks directory
                    UtilClass.saveImage(bookmark, UtilClass.getBookmarksDirectory(), "bookmark");
                    break;
            }
            //get a new suggestion if any of the click events are performed
            //this function will only be called if the click events are performed
            //i.e. new suggestion will only be generated if click events are performed
            newSuggestion();
        }
    }

}
