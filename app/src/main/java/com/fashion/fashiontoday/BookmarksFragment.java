package com.fashion.fashiontoday;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.File;

public class BookmarksFragment extends Fragment implements View.OnClickListener {

    //Buttons for sharing and deleting bookmarks
    Button shareButton, deleteButton;

    String path;
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;
    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bookmarks, container, false);

        //Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) view.findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getActivity());
        mPager.setAdapter(mPagerAdapter);

        //This limits number of pages to be loaded in the memory
        mPager.setOffscreenPageLimit(1);

        shareButton = (Button) view.findViewById(R.id.shareButton);
        deleteButton = (Button) view.findViewById(R.id.deleteButton);

        shareButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);

        return view;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void onClick(View v) {
        //This will check if the bookmarks directory is not empty then the click events will work
        if (!(UtilClass.isDirEmpty(UtilClass.getBookmarksDirectory()))) {

            //getting paths from page adapter
            //mPagerAdapter is down casted to ScreenSliderPagerAdapter
            path = ((ScreenSlidePagerAdapter) mPagerAdapter).getFilepaths().get(mPager.getCurrentItem()).toString();

            switch (v.getId()) {

                //Sharing the image
                case R.id.shareButton:

                    //get the uri of the image from the path
                    Uri imageUri = Uri.fromFile(new File(path));
                    Intent shareIntent = new Intent();

                    //add send action in the intent
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                    shareIntent.setType("image/jpeg");

                    //Chooser gets all the apps that can perform send action
                    startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.share_image)));
                    break;

                case R.id.deleteButton:
                    //Delete the bookmark from the path
                    (new File(path)).delete();

                    //notify the adapter that the bookmark is deleted
                    mPagerAdapter.notifyDataSetChanged();
                    break;
            }
        }
    }
}

