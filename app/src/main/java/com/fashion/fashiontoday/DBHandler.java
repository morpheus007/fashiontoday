package com.fashion.fashiontoday;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHandler extends SQLiteOpenHelper {

    //The table name and column names
    public static final String TABLE_USER = "user";
    public static final String COLUMN_USERID = "_id";
    public static final String COLUMN_USERNAME = "userName";
    public static final String COLUMN_PASSWORD = "password";

    //Initialize Database
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "fashion.db";


    public DBHandler(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Create table with autoincrement
        String query = "CREATE TABLE " + TABLE_USER + "(" +
                COLUMN_USERID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_USERNAME + " TEXT, " + COLUMN_PASSWORD + " TEXT" + ");";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //If version of database is changed tben drop the table
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        //If table is dropped then new blank table will be created
        onCreate(db);
    }

    //Add a new row to database
    public void addUser(User user) {

        //Put values from user object to database
        ContentValues values = new ContentValues();
        values.put(COLUMN_USERNAME, user.getUserName());
        values.put(COLUMN_PASSWORD, user.getPassword());


        SQLiteDatabase db = getWritableDatabase();

        //Insert the content values in the database
        db.insert(TABLE_USER, null, values);
        db.close();
    }


    //Print out the database as String
    public String databaseToString() {
        //The string should be initiazed to null
        String doString = "";

        //Get Database
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_USER + ";";

        //Current point of location in results
        Cursor c = db.rawQuery(query, null);

        //Move to First row in results
        c.moveToFirst();

        //To Check if the cursor is not pointing to end of result
        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex(COLUMN_USERNAME)) != null) {

                //Only get the username of the string which is the first element in cursor
                doString += c.getString(1);
                doString += "\n";
                c.moveToNext();
            }
        }
        //Close database and cursor
        db.close();
        c.close();
        return doString;
    }
    public void deleteUser(){
        SQLiteDatabase db=getWritableDatabase();

        //Delete user row from database
        db.execSQL("DELETE FROM " + TABLE_USER+";");
    }
}

