package com.fashion.fashiontoday;


import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/*This is the singleton class who's object is only initialized once throughout the application life cycle
A single instance of this class is created.In this class the important functions that are
required by multiple classes are static and they can be accessible from the any class*/

public class UtilClass extends Application {
    private static Context mContext;

    //Get all the directories and files at the start of the app
    private static File shirtsDirectory, pantsDirectory, bookmarksDirectory, file;
    private static UtilClass instance = new UtilClass();

    //This array list will store the combinations of shirts and pants that have occurred
    private static ArrayList<String> fileCombinations = new ArrayList<>();

    private UtilClass() {
    }

    //This function reads the array list data from the file using object stream
    protected static void readFile(File file) {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);

            //Read the array list object from file and add it to array list
            fileCombinations = (ArrayList<String>) ois.readObject();

        } catch (Exception e) {

            e.printStackTrace();
        } finally {

            try {
                if (fis != null)
                    fis.close();
                if (ois != null)
                    ois.close();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }

    }

    //Write the new data on the file
    protected static void writeFile(String string) {
        String filename = file.getName();

        FileOutputStream outputStream = null;
        ObjectOutputStream oos = null;

        try {
            //Add the new combination
            fileCombinations.add(string);
            outputStream = mContext.openFileOutput(filename, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(outputStream);
            oos.writeObject(fileCombinations);
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (outputStream != null)
                    outputStream.close();
                if (oos != null)
                    oos.close();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

    public static UtilClass createInstance(Context context) {
        mContext = context;

        //get all the required directories
        shirtsDirectory = mContext.getExternalFilesDir("/Shirts/");
        pantsDirectory = mContext.getExternalFilesDir("/Pants/");
        bookmarksDirectory = mContext.getExternalFilesDir("/Bookmarks/");

        // make all the directories if not present
        makeDirectories();

        //This will get the file which contains the list of combinations of shirts and pants
        //from the context if the file is not present then a new file will be created
        file = new File(mContext.getFilesDir(), "combinations");

        //Read from the file
        readFile(file);

        return instance;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static void makeDirectories() {

        //Make directories if they don't exist
        if (!shirtsDirectory.exists())
            shirtsDirectory.mkdirs();

        if (!pantsDirectory.exists())
            pantsDirectory.mkdirs();

        if (!bookmarksDirectory.exists())
            bookmarksDirectory.mkdirs();
    }

    protected static ArrayList<String> getFileCombinations() {
        return fileCombinations;
    }


    protected static File getShirtsDirectory() {
        return shirtsDirectory;
    }

    protected static File getPantsDirectory() {
        return pantsDirectory;
    }

    protected static File getBookmarksDirectory() {
        return bookmarksDirectory;
    }

    //This function saves the image in a given directory with given type and returns the square formatted bitmap image
    protected static Bitmap saveImage(Bitmap image, File directory, String type) {
        //The name of the image is given a data and time stamp so that a unique instance of image is created
        String name = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss", Locale.US).format(new Date());

        //Add the name and type and combine the strings
        type = type + name + ".jpg";

        File imageFile = new File(directory, type);
        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(imageFile);

            //Get a square formatted image
            image = squareBitmap(image);

            //Compress the image
            image.compress(Bitmap.CompressFormat.JPEG, 20, fos);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Make toast as the image file is created or failed to create
            if (imageFile.exists()) {
                Toast.makeText(mContext, mContext.getString(R.string.image_created) +
                        imageFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(mContext, R.string.image_not_created, Toast.LENGTH_SHORT).show();
        }
        return image;
    }

    //This function will create temporary image file which is stored in uncompressed format
    //which is then used to compress into required format and then deleted
    protected static File createImageFile(File directory, String type) throws IOException {

        //Create unique image by creating timestamp
        String name = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss", Locale.US).format(new Date());
        type = type + name;

        //Return Temporary file
        return File.createTempFile(
                type,  /* prefix */
                ".jpg",         /* suffix */
                directory      /* directory */
        );
    }

    //This function will resize bitmap to 800 by 800 pixels
    protected static Bitmap getResizedBitmap(Bitmap bm) {
        //Get the width and height of bitmap
        int width = bm.getWidth();
        int height = bm.getHeight();

        //Scale the width and height
        float scaleWidth = ((float) 800) / width;
        float scaleHeight = ((float) 800) / height;
        // Create a matrix for manipulation
        Matrix matrix = new Matrix();

        // Resize Bitmap
        matrix.postScale(scaleWidth, scaleHeight);

        //Recreate the new bitmap
        return Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
    }


    //This function will convert the image into square dimensions by adding extra white background
    protected static Bitmap squareBitmap(Bitmap bm) {
        int maxDim;
        Bitmap background;

        //Get max dimension value from width and height
        maxDim = Math.max(bm.getHeight(), bm.getWidth());

        //Create a square bitmap by using the
        background = Bitmap.createBitmap(maxDim, maxDim, Bitmap.Config.ARGB_8888);

        //Draw a canvas of the square bitmap
        Canvas canvas = new Canvas(background);
        canvas.drawColor(Color.WHITE);

        //subtract the width and height from max dimension value and then divide by 2 to plave in the centre
        canvas.drawBitmap(bm, (maxDim - bm.getWidth()) / 2, (maxDim - bm.getHeight()) / 2, null);

        //resize the bitmap
        background = getResizedBitmap(background);

        return background;

    }

    //This function will combine 2 bitmaps side by side
    protected static Bitmap combineBitmap(Bitmap bm1, Bitmap bm2) {
        int maxDim;
        Bitmap background;

        //This will get the maximum dimension value by comparing the height and sum of the widths of the 2 bitmaps
        //As the images are combined side by side we compare individual heights and sum of widths
        maxDim = Math.max(bm1.getHeight(), Math.max(bm2.getHeight(), bm1.getWidth() + bm2.getWidth()));
        background = Bitmap.createBitmap(maxDim, maxDim, Bitmap.Config.ARGB_8888);

        //Draw canvas of the bitmap
        Canvas canvas = new Canvas(background);
        canvas.drawColor(Color.WHITE);

        //Center the bitmap by subtracting width and height from max dimension value and divide by 2
        //Place the bitmap on the left most position of the canvas
        canvas.drawBitmap(bm1, (maxDim - bm1.getWidth() - bm2.getWidth()) / 2, (maxDim - bm1.getHeight()) / 2, null);

        //Center the bitmap by subtracting width and height from max dimension value and divide by 2
        //After centering the bitmap place another place it to the side of first bitmap
        //by adding the width of the first bitmap to the x position of this bitmap
        canvas.drawBitmap(bm2, (maxDim - bm1.getWidth() - bm2.getWidth()) / 2 + bm1.getWidth(), (maxDim - bm2.getHeight()) / 2, null);

        return background;

    }

    //This function will check if the directory is empty or not
    protected static boolean isDirEmpty(File directory) {
        ArrayList<String> fileDirs = new ArrayList<>();

        //This will traverse through the directory and add the path of files
        //in the array. If the array is empty then the directory is empty
        for (File file : directory.listFiles()) {
            fileDirs.add(file.getAbsolutePath());
        }

        return fileDirs.isEmpty();
    }
}
