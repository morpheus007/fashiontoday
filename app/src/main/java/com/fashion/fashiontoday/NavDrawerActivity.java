package com.fashion.fashiontoday;


import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class NavDrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int HOME_FRAGMENT = 0;
    TextView headerText;

    //Setup Drawer Layout
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_drawer);

        //Setup ActionBar Drawer Toggle
        ActionBarDrawerToggle mDrawerToggle;

        //Setup NavigationView and Toolbar
        NavigationView mNavigationView;
        Toolbar toolbar;

        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set DrawerLayout and ActionBarDrawerToggle
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

                //This will stop the hamburger animation
                super.onDrawerSlide(drawerView, 0);
            }
        };

        //Setup a drawer listener for ActionDrawerToggle
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        //This will show the hamburger icon to the top right
        mDrawerToggle.syncState();

        //Set NavigationView
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        //get navigation drawer header text
        headerText = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.header_text);

        //get data from database
        DBHandler dbHandler = new DBHandler(this, null);

        //set header text from the data received from database
        headerText.setText(dbHandler.databaseToString());

        //set navigation item listener
        mNavigationView.setNavigationItemSelectedListener(this);

        //This command will keep one item from NavDrawer always checked
        mNavigationView.getMenu().getItem(HOME_FRAGMENT).setChecked(true);

        //Open Suggestions by default every time the user opens the app
        openFragment(new SuggestionsFragment());
    }

    @Override
    public void onBackPressed() {

        //This will check if the drawer is opened or not
        //If the drawer was opened then the drawer will close on back button pressed
        //If the drawer was not opened then the activity will finish on back button pressed
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exitButton:
                finish();
                break;
            case R.id.action_settings:
                break;
        }
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        //create fragments for NavDrawer items
        Fragment fragment = null;

        // Handle navigation view item clicks here.
        //Clicking on navigation item will replace the current fragment on the screen
        switch (item.getItemId()) {
            case R.id.suggesstionsItem:
                fragment = new SuggestionsFragment();
                break;
            case R.id.shirtsItem:
                fragment = new ShirtsFragment();
                break;
            case R.id.pantsItem:
                fragment = new PantsFragment();
                break;
            case R.id.bookmarksItem:
                fragment = new BookmarksFragment();
                break;

        }
        if (fragment != null) {
            openFragment(fragment);

        }
        //This will highlight the item that is selected from NavDrawer
        item.setChecked(true);

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    //This function will open a new fragment and replace the current fragment
    public void openFragment(Fragment fragment) {

        //replace fragment by using fragmentManager
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_content, fragment).commit();
    }
}

