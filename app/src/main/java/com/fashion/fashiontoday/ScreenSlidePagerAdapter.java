package com.fashion.fashiontoday;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;



//A simple pager adapter that represents Images, in sequence
public class ScreenSlidePagerAdapter extends PagerAdapter {

    Context context;

    //LayoutInflater creates the layout for images
    LayoutInflater layoutInflater;
    ImageView bookmarksImage;

    private List<String> filepaths;
    private File bookmarksDirectory;

    public ScreenSlidePagerAdapter(Context context) {
        this.context = context;

        //We take images from this directory and display in our view pager
        bookmarksDirectory = UtilClass.getBookmarksDirectory();

        filepaths = new ArrayList<>();

        //Get file paths from the directory and store in ArrayList
        for (File file : bookmarksDirectory.listFiles()) {
            filepaths.add(file.getAbsolutePath());
        }
    }

    //This function will return the count of pages in the adapter
    @Override
    public int getCount() {
        return bookmarksDirectory.listFiles().length;
    }


    public List getFilepaths() {
        return filepaths;
    }

    //The page item is created in this function
    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        //LayoutInflater service is instantiated
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //From LayoutInflater service the view of the page is created
        //The path of the layout xml is given
        View itemView = layoutInflater.inflate(R.layout.screen_slide_fragment, container, false);

        //Setup the ImageView and set the bitmap from the path
        bookmarksImage = (ImageView) itemView.findViewById(R.id.bookmarksImage);
        bookmarksImage.setImageBitmap(BitmapFactory.decodeFile(filepaths.get(position)));

        //The view is added in the container where it can be displayed
        container.addView(itemView);

        return itemView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    //This will destroy the view if the view is not focused
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    //If there is any change in the directory or the pager data this function is called
    @Override
    public void notifyDataSetChanged() {

        //Refresh the file paths ArrayList
        //Delete all elements in the ArrayList
        filepaths.clear();

        //Get the new list of file paths from the directory and save it in ArrayList
        for (File file : bookmarksDirectory.listFiles()) {
            filepaths.add(file.getAbsolutePath());
        }
        super.notifyDataSetChanged();
    }

    //This will return position null if the there are no items in the pager
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}