package com.fashion.fashiontoday;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;


public class PantsFragment extends Fragment implements View.OnClickListener {

    //These are the request codes for getting the photo from camera or from gallery
    private static final int PANT_CAMERA = 0;
    private static final int PANT_GALLERY = 1;

    //Setup buttons to get photo from camera or gallery
    Button getPantButtonCamera, getGetPantButtonGallery;
    ImageView pantPhoto;

    // Create the File where the photo should go
    File photoFile = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Inflate the view with the layout
        View view = inflater.inflate(R.layout.pants, container, false);

        pantPhoto = (ImageView) view.findViewById(R.id.pantPhoto);

        getPantButtonCamera = (Button) view.findViewById(R.id.getPantButtonCamera);
        getGetPantButtonGallery = (Button) view.findViewById(R.id.getPantButtonGallery);

        getPantButtonCamera.setOnClickListener(this);
        getGetPantButtonGallery.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.getPantButtonCamera:

                //This function will return the photo from cam
                dispatchTakePictureIntent();
                break;

            case R.id.getPantButtonGallery:

                //get photo from gallery
                Intent intent = new Intent();
                //photo type is image
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        getActivity().getString(R.string.select_picture)), PANT_GALLERY);
                break;
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Bitmap image = null;

            //Request is for camera
            if (requestCode == PANT_CAMERA) {

                //Get the path of the temporary photo file
                image = BitmapFactory.decodeFile(photoFile.getAbsolutePath());

                //delete the temporary file as it is saved in bitmap
                photoFile.delete();

                //This will save the image inside the directory
                image = UtilClass.saveImage(image, UtilClass.getPantsDirectory(), "pant");


            }
            //Request is for gallery
            else {
                try {
                    //get image from gallery
                    image = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());

                    //This will save the image inside the directory
                    image = UtilClass.saveImage(image, UtilClass.getPantsDirectory(), "pant");

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            //setup image view from bitmap
            pantPhoto.setImageBitmap(image);
        } else
            //The temporary file will be created even if result is found so we need to delete it
            photoFile.delete();
    }

    //This function will take a full uncompressed photo and store it in temporary file and then decode it
    //while decoding it will compress it to the desired image and then delete the temporary file
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            try {

                //This will create a temporary image file by using the static function from the singleton UtilClass
                photoFile = UtilClass.createImageFile(UtilClass.getPantsDirectory(), "pant");

            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                //It takes the uri of the file and stores it in intent
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, PANT_CAMERA);
            }
        }
    }

}
