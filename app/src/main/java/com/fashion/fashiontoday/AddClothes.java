package com.fashion.fashiontoday;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

//This Activity is called if the user just signs up for the first time
//It will allow the user to add shirts and pants before using the main app
public class AddClothes extends AppCompatActivity {

    //Setup Fragments for pant and shirt
    ShirtsFragment shirtsFragment;
    PantsFragment pantsFragment;

    Button getSuggestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_clothes);

        //Setup the fragments using fragment manager
        shirtsFragment = (ShirtsFragment) getSupportFragmentManager().findFragmentById(R.id.shirtsFragment);
        pantsFragment = (PantsFragment) getSupportFragmentManager().findFragmentById(R.id.pantsFragment);
        getSuggestion = (Button) findViewById(R.id.getSuggestion);

        getSuggestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Check if the shirts or pants directory aren't empty
                boolean shirtEmpty, pantEmpty;

                //Call directory checker for checking the directory if they are empty
                shirtEmpty = UtilClass.isDirEmpty(UtilClass.getShirtsDirectory());
                pantEmpty = UtilClass.isDirEmpty(UtilClass.getPantsDirectory());

                //If any of the shirts or pants directories are empty then call AddClothes Activity
                //Else it will call the main activity i.e. the NavDrawer activity
                if (!(shirtEmpty || pantEmpty)) {
                    Intent addClothes = new Intent(AddClothes.this, NavDrawerActivity.class);
                    finish();
                    startActivity(addClothes);
                } else {
                    Toast.makeText(AddClothes.this, R.string.add_photos, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
