package com.fashion.fashiontoday;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class StartScreen extends AppCompatActivity {
    long checkStart, checkFinish = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_screen);

        //Create the instance of the singleton class at the start of the app
        UtilClass.createInstance(this);

        //This will show the StartScreen and hold it for 3 seconds
        Thread timer = new Thread() {
            synchronized
            public void run() {
                try {
                    checkStart = System.currentTimeMillis();
                    sleep(3000);
                } catch (InterruptedException e) {
                    finish();
                } finally {
                    if (checkFinish < checkStart) {

                        //Create a database handler object to access database
                        DBHandler dbHandler = new DBHandler(StartScreen.this, null);

                        //This will check if the user has already registered
                        //If thd user has registered the main NavDrawer activity will start
                        //Else the database will be null and the user will have to register
                        //Register Activity will open
                        if (!dbHandler.databaseToString().equals("")) {

                            //Check if the shirts or pants directory aren't empty
                            boolean shirtEmpty, pantEmpty;

                            //Call directory checker for checking the directory if they are empty
                            shirtEmpty = UtilClass.isDirEmpty(UtilClass.getShirtsDirectory());
                            pantEmpty = UtilClass.isDirEmpty(UtilClass.getPantsDirectory());

                            //If any of the shirts or pants directories are empty then call AddClothes Activity
                            //Else it will call the main activity i.e. the NavDrawer activity
                            if (shirtEmpty || pantEmpty) {
                                Intent addClothes = new Intent(StartScreen.this, AddClothes.class);
                                finish();
                                startActivity(addClothes);
                            } else {
                                Intent mainActivity = new Intent(StartScreen.this, NavDrawerActivity.class);
                                startActivity(mainActivity);
                            }
                        } else {
                            Intent registerActivity = new Intent(StartScreen.this, RegisterActivity.class);
                            startActivity(registerActivity);
                        }
                    }
                }
            }

        };
        timer.start();
    }

    @Override
    protected void onPause() {

        super.onPause();
        //This will check if the app is closed before calling next activity
        checkFinish = System.currentTimeMillis();

        //This will finish the entire app before starting as if the user opens this app by mistake
        finish();
    }
}

